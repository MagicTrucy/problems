#!/usr/bin/python3
"""Reads standard input for question mark separated sentences and say if
   they're anagrams"""

if __name__ == '__main__':
    try:
        LEFT, RIGHT = input().split("?")
    except ValueError:
        print("Input MUST contain ONE and only one question mark")
        exit(1)

    def are_words_scrambled(text1, text2):
        """Returns true if text1 and text2 words are the same"""
        text1 = text1.replace("\"", "")
        text2 = text2.replace("\"", "")
        text1 = sorted(text1.split())
        text2 = sorted(text2.split())
        return text1 == text2

    def is_anagram(text1, text2):
        """Returns true if text1 is an anagram of text2"""
        text1 = sorted(text1.replace(" ", "").lower())
        text2 = sorted(text2.replace(" ", "").lower())
        return text1 == text2

    if is_anagram(LEFT, RIGHT) and not are_words_scrambled(LEFT, RIGHT):
        CONDITION = ""
    else:
        CONDITION = " NOT"

    print(LEFT + "is" + CONDITION + " an anagram of" + RIGHT)
